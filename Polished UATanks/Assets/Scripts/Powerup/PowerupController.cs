﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    public List<Powerup> powerups;//Creates our power up list variable
    private TankData tankData;//grabs the TankData script
    // Use this for initialization
    void Start()
    {
        powerups = new List<Powerup>();//starts our list for the powerups
        tankData = GetComponent<TankData>();//Grabs our TankData script
    }

    // Update is called once per frame
    void Update()
    {
        List<Powerup> expiredPowerups = new List<Powerup>();//Create a list for our expired powerups
        foreach (Powerup power in powerups)//for every Power in our powerups
        {
            power.duration -= Time.deltaTime;//if the duration has expired
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);//remove the powerup
            }
        }
        foreach (Powerup power in expiredPowerups)//when our powerup is ready to be removes
        {
            power.OnDeactivate(tankData);//deactivate the powerup
            powerups.Remove(power);//remove the powerup
        }
    }
    public void Add(Powerup powerup)//When we are ready to add a powerup
    {
        powerup.OnActivate(tankData);//activate the powerup
        if (!powerup.isPermanent)//If our powerup is not perminant
        {
            powerups.Add(powerup);//add it to our list of powerups
        }
    }
}
