﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLocationPowerup : MonoBehaviour {
    public GameObject PickupPrefab;//Allows the Powerup prefab to be set
    public float spawnDelay;//allows a spawn delay set for this prefab
    private float nextSpawnTime;//allows us to spawn in a new powerup
    private Transform tf;//helps grab the Transform Component
    private GameObject spawnedPickup;//Helps grab the spawned powerup
    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();//grabs the Transform component
        nextSpawnTime = Time.time;//Sets current spanw time as the current game time
	}
	
	// Update is called once per frame
	void Update () {
		if (spawnedPickup == null)//if nothing is spawned
        {
            if (Time.time > nextSpawnTime)//and it is time to respawn the powerup
            {
                spawnedPickup = Instantiate(PickupPrefab, tf.position, Quaternion.identity) as GameObject;//instantate the powerup as a game object
                nextSpawnTime = Time.time + spawnDelay;//next spawn takes place after this one
            }
        } else//unless it is still here
        {
            nextSpawnTime = Time.time + spawnDelay;//update the spawn delay.
        }
	}
}
