﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup
{
    public float speedModifier;//move speed buff
    public float fireRateModifier;//fire rate buff
    public bool shield;//shield buff
    public float duration;//how long will it last?
    public bool isPermanent;//is it perminant?

    //when the buff is activated
    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedModifier;
        target.fireRate += fireRateModifier;
        target.shield = true;
    }
    //When the buff is done
    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.fireRate -= fireRateModifier;
        target.shield = false;
    }
}
