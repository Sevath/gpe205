﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Powerup powerup;//allows us to set out Powerup script
    private Transform tf;//Allows us to grab your Transform Component

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();//This will grab our Transform component
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerEnter(Collider other)//When something enters
    {
        PowerupController Powcon = other.GetComponent<PowerupController>();//Create a PowerupController variable
        if (Powcon != null)//If it exists
        {
            Powcon.Add(powerup);//Add our current powerup
            if (GameManager.instance.feedback != null)//If sound exists
            {
                AudioSource.PlayClipAtPoint(GameManager.instance.feedback, tf.position, GameManager.instance.fxSound.value);//play the sound at the location
            }
            Destroy(gameObject);//destroy the powerup
        }
    }
}
