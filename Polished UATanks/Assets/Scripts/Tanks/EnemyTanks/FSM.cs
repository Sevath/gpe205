﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour {
    private Transform tf;//holds current GameObject Transform Component
    private TankData data;//Holds the TankData script
    private TankMotor motor;//Holds the Tank Motor script
    private AiAttackMode AiTank;//allows us to grab our AiAttackMode script
    public GameManager gm;//Allows us to grab the game manager
    public AiSenses senses;//allows us to grab the AiSenses script
    private float exitTime;//private exit time for avoidance.
    public int avoidanceStage = 0;//allows programmer to set the avoidance stage we will be in
    // Use this for initialization
    void Start() {
        tf = GetComponent<Transform>();//Grabs the Transform component
        data = GetComponent<TankData>();//grabs the TankData script
        motor = GetComponent<TankMotor>();//grabs the TankMotor script
        AiTank = GetComponent<AiAttackMode>();//grabs teh AiAttackMode script
        senses = GetComponent<AiSenses>();//grabs teh AiSenses script.
    }

    // Update is called once per frame
    void Update() {
        
	}
    public void Flee(Transform target)
    {
        Vector3 vectorToTarget = target.position - tf.position;//how far away are we from the target?
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;//move away from the target.
        vectorAwayFromTarget.Normalize();
        vectorAwayFromTarget *= AiTank.fleeDistance;
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.rotateSpeed);//rotate away from the target
        motor.Move(data.moveSpeed);//move forward
    }
    public void DoChase(Transform target)//When we need to chase
    {
        motor.RotateTowards(target.position, data.rotateSpeed);//rotate to the target
        if (CanMove(data.moveSpeed))//if nothing is infront of us
        {
            motor.Move(data.moveSpeed);//move forward
        }
        else//otherwise
        {
            avoidanceStage = 1;//Change the avoidance stage to 1
        }
    }

    public void DoAvoidance()//when we need to avoid an obsticle
    {
        if (avoidanceStage == 1)//if our avoidance is at stage 1
        {
            motor.Rotate(-1 * data.rotateSpeed);//rotate away from the object
            if (CanMove(data.moveSpeed))//if we can move now, proceed
            {
                avoidanceStage = 2;//avoidance stage 2
                exitTime = AiTank.avoidanceTime;//our exit time now equals AiTank.avoidanceTime;
            }
        }
        else if (avoidanceStage == 2)//if we are alread in stage 2 of avoidance
        {
            if (CanMove(data.moveSpeed))//and we can move forward
            {
                exitTime -= Time.deltaTime;//keep moving until we have moved the allocated exit time 
                motor.Move(data.moveSpeed);//move forward
                if (exitTime <= 0)//once we do not need to move forward again
                {
                    avoidanceStage = 0;//avoidance stage is 0
                }
            }
            else//otherwise
            {
                avoidanceStage = 1;//set our avoidance stage to 1
            }
        }
    }
    public bool CanMove(float speed)//Can we move?
    {
        RaycastHit hit;//Create a ray infront of us
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))//if our ray hit something.
        {
            if (!hit.collider.CompareTag("Player"))//and it is NOT the player
            {
                return false;//return false
            }
        }
        return true;//otherwise we can move.
    }
    public bool DoFlee()//Are we going to flee?
    {
        //TODO: add flee functioin
        return false;
    }
}

