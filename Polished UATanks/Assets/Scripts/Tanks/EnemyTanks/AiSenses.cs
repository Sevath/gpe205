﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiSenses : MonoBehaviour
{
    //Hearing distance
    //viewing distance
    public float fieldOfView = 45.0f;//enemy tank set vield of view
    public Transform target;//allows setting of player as target
    private Transform tf;//allows grabbing of enemy Tansform component
    // Use this for initialization
    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        tf = GetComponent<Transform>();//grabs our Transform component
    }

    // Update is called once per frame
    void Update()
    {

    }
    public bool CanSee(Transform target)//Can we see the player?
    {
        Vector3 agentToTargetVector = target.position - GetComponent<Transform>().position;
        float angleToTarget = Vector3.Angle(agentToTargetVector, transform.forward);
        if (angleToTarget < fieldOfView)//if the target is in our angle of view.
        {
            RaycastHit hit;//Create our ray.
            if (Physics.Raycast(tf.position, agentToTargetVector, out hit))//Looking at our ray infront of us.
            {
                if (!hit.collider.CompareTag("Player"))//If something is blocking our view
                {
                    return false;//We can not see the player
                }
            }
        }
        else//otherwise
        {
            return false;//Can not see the player.
        }
        return true;//We see the player.
    }
}
