﻿//USE THIS SCRIPT FOR TESTING REASONS!!!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiAttackMode : MonoBehaviour
{
    private FSM Ai;//Holds our FSM state.
    public enum AttackMode { Chase, Flee, Patrol, none };//Developer can choose attack mode
    public AttackMode attackMode;//sets the attack mode as attack mode
    public Transform target;//allows us to manually set the target
    private Transform targetPlaceHolder;//Placeholder for our target variable
    private Transform tf;//Holds our game Objects Transform component
    public Transform[] waypoints;//allows addition of waypoints for tank patrol
    private int currentWaypoint = 0;//sets first waypoint as 0
    public float closeEnough = 1.0f;//allows set close enough before next waypoint
    public float fleeDistance = 1.0f;//Publicly sets the fleeDistance we will run
    public float avoidanceTime = 2.0f;//publicly allows how far in the future we will look into

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();//Grabs the Transform component
        Ai = GetComponent<FSM>();//Grabs the FSM Script
        targetPlaceHolder = GameObject.FindWithTag("Player").transform;//Sets our target palceholder, to the main game target.
    }

    // Update is called once per frame
    void Update()
    {
            if (Ai.senses.CanSee(Ai.senses.target) == true)//If we can see the player
            {
                if (Ai.DoFlee() == true)//and the player does not need to reload
                {
                    attackMode = AttackMode.Flee;//run away from the player
                }
                else if (Ai.DoFlee() == false)//if the player needs to reload
                {
                    attackMode = AttackMode.Chase;//chase the player
                }
            }
            else//otherwise
            {
                attackMode = AttackMode.Patrol;//Continue patroling.
            }
            if (attackMode == AttackMode.Chase)//If we are going to chase
            {
                target = targetPlaceHolder;//Our current target, is the player.
                if (Ai.avoidanceStage != 0)//and our avoidance stage is not 0
                {
                    Ai.DoAvoidance();//avoid the obsticle
                }
                else//otherwise
                {
                    Ai.DoChase(target);//chase the target
                }
            }
            else if (attackMode == AttackMode.Flee)//If our attack mode is flee
            {
                Ai.Flee(target);//Initiate the flee command.
            }
            else if (attackMode == AttackMode.Patrol)//If enemy tank is going to patrol.
            {
                if (target = targetPlaceHolder)//and our target is the current player
                {
                    target = waypoints[currentWaypoint];//change the target to the current waypoint on our patrol list.
                }
                if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))//if we are close enough to our next waypoint
                {
                    currentWaypoint++;//proceed to the next waypoint.
                    if (currentWaypoint == waypoints.Length)//if our waypoint hits above max
                    {
                        currentWaypoint = 0;//reset our movement.
                        target = waypoints[currentWaypoint];//our target is set as the first waypoint.
                    }
                    else//otherwise
                    {
                        target = waypoints[currentWaypoint];//our target starts off as the waypoint we are on.
                    }
                }
                {
                    if (Ai.avoidanceStage != 0)//and our avoidance stage is not 0
                    {
                        Ai.DoAvoidance();//avoid the obsticle
                    }
                    else//otherwise
                    {
                        Ai.DoChase(target);//chase the target
                    }
                }
            }
        }
    }
   
