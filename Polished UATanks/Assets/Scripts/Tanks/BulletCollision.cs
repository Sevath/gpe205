﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{

    void Start()
    {

    }
    

    // Update is called once per frame
    void Update()
    {

    }
    public void OnCollisionEnter(Collision collision)//On collision of the bullet
    {
        switch (collision.gameObject.tag)
        {//What do we collide with?

            case ("EnemyTank")://if we collide with enemy tank
                AudioSource.PlayClipAtPoint(GameManager.instance.bulletExplosion, this.GetComponent<Transform>().position, GameManager.instance.fxSound.value);//play the sound at the location
                Destroy(this.gameObject);//Destroy bullet
                Destroy(collision.gameObject);//Destroy tank
                AudioSource.PlayClipAtPoint(GameManager.instance.death, collision.gameObject.GetComponent<Transform>().position, GameManager.instance.fxSound.value);//Plays the audiosource, death, at the collision.
                switch (GameManager.instance.player)//This decides who shot, to get the point.
                {
                    case (0)://if player one shot
                        GameManager.instance.playerOneScore++;//increase player one score.
                        GameManager.instance.playerOneScoreText.text = "Score: " + GameManager.instance.playerOneScore;//Update player one score on gui
                        break;
                    case (1)://if player two shot
                        GameManager.instance.playerTwoScore++;//update player two score
                        GameManager.instance.playerTwoScoreText.text = "Score " + GameManager.instance.playerTwoScore;//update player two score on gui
                        break;
                }
                break;
            case ("Wall"):// if we collide with wall
                Destroy(this.gameObject);//destroy bullet.
                AudioSource.PlayClipAtPoint(GameManager.instance.bulletExplosion, this.GetComponent<Transform>().position, GameManager.instance.fxSound.value);//play the sound at the location
                break;
        }
    }
}
