﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {
    //put variables here
    private CharacterController characterController;//Creates a variable for the CharacterController
    public Transform tf;//creates a vriable for the Transform
    public TankData data;//creates a variable for TankData script.
	// Use this for initialization
	void Start () {
        characterController = gameObject.GetComponent<CharacterController>();//Grabs the CharacterController.
        tf = gameObject.GetComponent<Transform>();//Grabs the Transform Component
        data = gameObject.GetComponent<TankData>();//Grabs our TankData component
	}
	
	// Update is called once per frame
	void Update () {

	}
    //Self created functions below.
    public void Move(float speed)//Creates a function for moving forward
    {
        Vector3 speedVector = tf.forward;//Creates a speed vector, and places the facing for forward of the object.
        characterController.SimpleMove (speedVector *= speed);//applys the speed value, and concerts it to meters per second.
    }
    public void Rotate(float speed)//Create a function for roation
    {
        Vector3 rotateVector = Vector3.up;//sets the roation by one degree per frame.
        rotateVector *= speed * Time.deltaTime;//Adjusts rotation based on speed changes rotation to speed instead of frames.
        tf.Rotate(rotateVector, Space.Self);//rotates the tank by the set value.
    }
    public bool RotateTowards(Vector3 target, float speed)//When to rotate and what to do during/after.
    {
        Vector3 vectorToTarget = target - tf.position;//how far is the next waypoint to look at.
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);//rotate to the next waypoint.
        if (targetRotation == tf.rotation)//if we are done rotating
        {
            return false;//return false
        }
        else
        {
            tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.rotateSpeed * Time.deltaTime);//how long it needs to take to rotate to next waypoints.
            return true;//return true, keep rotating
        }
    }   
}
