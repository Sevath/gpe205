﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public enum InputScheme { WASD, arrowKeys };//Allows for input to be WASD or Arrow Keys
    public InputScheme input = InputScheme.WASD;//Defaults the movement to WASD
    public TankData data;//Allows us to set our TankData script
    public TankMotor motor;//Allows us to set our TankMotor script
    public int player;//Allows us to set our player
    public Transform tf;
    public Canvas canvas;
    public Camera cam;
    public GameObject bullet;//Allows the bullet object to be set
    // Use this for initialization
    void Start()
    {
        
        data = GetComponent<TankData>();//Auto sets our TankData
        motor = GetComponent<TankMotor>();//Auto sets our TankMotor
    }

    // Update is called once per frame
    void Update()
    {

        //This is a cheatsheat for lives.
        if (Input.GetKeyDown(KeyCode.H))
        {
            GameManager.instance.TakeALife(0);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            GameManager.instance.TakeALife(1);
        }


        switch (input)//If the input is....
        {
            case InputScheme.arrowKeys://if Input is the arrow keys
                player = 1;
                if (Input.GetKey(KeyCode.UpArrow))//if the user presses the Up Arrow
                {
                    motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.DownArrow))//if the user presses the down arrow
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.RightArrow))//if the user presses the right arrow 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.LeftArrow))//if the user presses the left arrow
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                if (Input.GetKeyDown(KeyCode.Keypad0))//if user presses space
                {
                    if (Time.time > GameManager.instance.reloadDelay + GameManager.instance.timeHolderP1)//If we are able to shoot
                    {
                        GameManager.instance.player = player;
                        AudioSource.PlayClipAtPoint(GameManager.instance.shooting, tf.position, GameManager.instance.fxSound.value);//We will play the shooting audio at our tank.
                        GameObject newBullet = Instantiate(bullet, tf.position, tf.rotation) as GameObject;//summons the bullet
                        GameManager.instance.timeHolderP1 = Time.time;//updates last bullet fire time
                        newBullet.GetComponent<Rigidbody>().velocity = GameManager.instance.bulletSpeed * tf.forward;//fires the bullet at designated designer speed
                        Debug.Log(player);//Logs which player shoots
                        Destroy(newBullet, GameManager.instance.bulletDecay);//destroys bullet after 5 seconds
                    }
                    else if (Time.time < GameManager.instance.reloadDelay + GameManager.instance.timeHolderP1)//if user presses space and the cannon is not ready
                    {
                        //Do nothing.
                    }
                }
                break;
            case InputScheme.WASD:
                player = 0;
                if (Input.GetKey(KeyCode.W))//if the user presses W
                {
                    motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.S))//if the user presses S
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.D))//if the user presses D 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.A))//if the user presses A
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                if (Input.GetKeyDown(KeyCode.Space))//if user presses space
                {
                    if (Time.time > GameManager.instance.reloadDelay + GameManager.instance.timeHolderP1)//if we are able to shoot.
                    {
                        GameManager.instance.player = player;
                        AudioSource.PlayClipAtPoint(GameManager.instance.shooting, tf.position, GameManager.instance.fxSound.value);//We will play the shooting audio clip at our tank.
                        GameObject newBullet = Instantiate(bullet, tf.position, tf.rotation) as GameObject;//summons the bullet
                        GameManager.instance.timeHolderP1 = Time.time;//updates last bullet fire time
                        newBullet.GetComponent<Rigidbody>().velocity = GameManager.instance.bulletSpeed * tf.forward;//fires the bullet at designated designer speed
                        Debug.Log(player);//Logs which player shoots
                        Destroy(newBullet, GameManager.instance.bulletDecay);//destroys bullet after 5 seconds
                    }
                    else if (Time.time < GameManager.instance.reloadDelay + GameManager.instance.timeHolderP1)//if user presses space and the cannon is not ready
                    {
                        //Do nothing.
                    }
                }
                break;
        }
    }
}
