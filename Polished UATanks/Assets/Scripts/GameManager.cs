﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    //variables for instancing our player
    public GameObject playerPrefab;//allows us to set our player one prefab
    public GameObject player2Prefab;//allows us to set our player two prefab
    private GameObject playerSpawn;//allows us to spawn our player randomly in the generating grid

    //Variables for our bullet
    public GameObject bullet;//Allows the bullet object to be set
    public float timeHolderP1;//holds time last bullet was fired. 
    public float timeHolderp2;//holds the last time player 2 shot.
    public float bulletSpeed;//holds bullet speed
    public float bulletDecay;//Public variable for when the bullet will despawn.
    public Transform playerOneBullet;//this is where player one will shoot form
    public Transform playerTwoBullet;//this is where player two will shoot from
    public float reloadDelay;//holds the timer to reload
    public int player;//this holds our player number

    //Room Variables
    public int rows;//Designer set rows
    public int cols;//Designer set columns
    public float roomWidth = 50.0f;//Designer set how wide our rooms are
    public float roomHeight = 50.0f;//Designer set how high our rooms are
    public GameObject[] gridPrefabs;//Allows us to set which tiles we will use in our map
    private Room[,] grid;//saves a list of our current map tiles we created later on
    public enum mapGeneration { Random, Preset, TimeOfDay };//Developers can set which kind of map they wish to spawn
    public mapGeneration mapGen = mapGeneration.Preset;//auto sets it for developer Preset
    public int mapSeed;//Allows Developer Preset Seed

    //helps with spawning in enemys and ourselves randomly.
    public int MaxEnemy;//Max enemies allowed
    private int CurEnemy;//current amount of enemies
    private int maxPlayer;//Max players summond is false
    public Transform PlayerTank;//Allows us to set the PlayerTank Transform component

    //Player Variables
    private bool multiplayer = false;//allows code to set multiplayer value;
    public Dropdown multiplayerInt;//allows us to choose how many people will play in inspectator.
    public GameObject playerOne;//allows manual set of playerOne game object
    public GameObject playerTwo;//allows manual set of playerTwo game object
    //Player lives
    public int playerOneLives;//allows us to set the starting lives of player one.
    public Text playerOneLivesText;//Allows us to tell the player how many lives they have
    private int playerOneLivesPlaceholder;//holds the starting value for the lives of player one
    public int playerTwoLives;//allows to set the number of lives for player two
    public Text playerTwoLivesText;//tells player two how many lives they have
    private int playerTwoLivesPlaceholder;//holds the starting value for the lives of player two.
    //Player scores
    public int playerOneScore;//allows us to se tthe starting score of player one
    public Text playerOneScoreText;//allows us to tell the player how many score they have
    private int playerOneScorePlaceholder;//allows us to hold the starting value for the score of player one
    public int playerTwoScore;//allows us to set the starting score of player two
    public Text playerTwoScoreText;//allows us to tell player two what there score is
    private int playerTwoScorePlaceholder;//holds the default value of player two score
    //player Shots
    public bool playerOneShot;//allows the program to know player one shot
    public bool playerTwoShot;//allows the program to know player two shot

    //canvas
    public GameObject startScreen;//holds the start screen menu
    public GameObject game;//holds the game value
    public GameObject options;//holds the options menu
    public GameObject howToPlay;//holds the how to play menu
    public GameObject MenuCamera;//holds the starting camera
    public GameObject playerOneGui;//holds player one's gui
    public GameObject playerTwoGui;//holds player two's gui
    public GameObject pauseMenu;//holds the pause menu
    public Dropdown mapMode;//allows us to hold the map mode value
    public GameObject gameover;//holds the game over screen

    //sounds
    public Slider musicSound;//background music volume level
    public Slider fxSound;//fx sound volume level
    public AudioClip feedback;//Powerup pickup
    public AudioClip bulletExplosion;//bullet explosion sound value
    public AudioClip menuMusic;//menu music value
    public AudioClip gameMusic;//in game music value
    public AudioClip clicking;//in game button clicking sound value
    public AudioClip shooting;//in game shooting sound value
    public AudioClip death;//death sound value


    // Use this for initialization our singleton GameManager
    void Awake() {
        if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this.gameObject);
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
    }
    //When the game starts
    void Start()
    {
        AudioSource.PlayClipAtPoint(menuMusic, MenuCamera.GetComponent<Transform>().position, musicSound.value);
    }

    void Update()
    {
        //if our player has 0 life 
        if (playerOneLives <= 0)
        {
            Destroy(playerOne);//destroy player one
        }
        if (playerTwoLives <= 0)
        {
            Destroy(playerTwo);//destory player two
        }
        if (playerOneLives <= 0 && playerTwoLives <= 0)//if both players are done
        {
            Gameover();//run game over
        }
        if (Input.GetKeyDown(KeyCode.Escape))//if user hits escape
        {
            pause();//pause the game.
        }
    }

    //adds all the time used for today to create a random map.
    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    //generates our map.
    public void GenerateGrid()
    {
        switch (mapGen)//This allows us to decide how random our map will truly be.
        {
            case mapGeneration.Preset://if developer set it as preset
                UnityEngine.Random.InitState(mapSeed);//use preset seed determained
                break;
            case mapGeneration.Random://if set as random
                UnityEngine.Random.InitState(DateToInt(DateTime.Now));//use all of the function DatetoInt to create random seed to spawn.
                break;
            case mapGeneration.TimeOfDay://if time of day.
                mapSeed = DateToInt(DateTime.Now.Date);//we will set our current seed as today using the function DateTime for now and todays date.
                UnityEngine.Random.InitState(mapSeed);//spawns the map using our new seed.
                break;
        }
        //Clear our grid
        grid = new Room[cols, rows];
        for (int i = 0; i < rows; i++)//for each grid row
        {
            for (int j = 0; j < cols; j++)//and for each grip column
            {
                //figure out which section in our grip
                float xPosition = roomWidth * j;//The position of our J component is...
                float zPosition = roomHeight * i;//The position of our I component is...
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);//Places our grid tile location
                //creates a random room from our prefabs at that location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;//instantiate our grid         
                tempRoomObj.transform.parent = this.transform;  //sets it as a parent
                tempRoomObj.name = "Room_" + j + "," + i;//names our new grip location.
                Room tempRoom = tempRoomObj.GetComponent<Room>();//grab our room object

                //this will set a random Bool for our enemy spawns.
                bool Boolean = (UnityEngine.Random.value > 0.5f);
                //if true and we hav eyet hit the max enemy limit
                if (Boolean == true && CurEnemy < MaxEnemy)
                {
                    //spawn in a new enemy
                    tempRoom.EnemySpawn.SetActive(true);
                    CurEnemy++;
                }
                //otherwise do not spawn our enemy.
                else
                {
                    tempRoom.EnemySpawn.SetActive(false);
                    //now do the same for our player since there is no enemy. //repeat but with our player selection. // only for sinlge player.
                    if (multiplayer == false && tempRoom.playerSpawn != null && maxPlayer == 0)
                    {
                        PlayerTank = tempRoom.playerSpawn.GetComponent<Transform>();
                        maxPlayer++;
                        GameObject playerTank = Instantiate(playerPrefab, PlayerTank.position, PlayerTank.rotation) as GameObject;
                        playerOne = playerTank;
                    }else if (multiplayer == true && tempRoom.playerSpawn != null && maxPlayer == 0)
                    {
                        PlayerTank = tempRoom.playerSpawn.GetComponent<Transform>();
                        maxPlayer++;
                        GameObject playerTank = Instantiate(playerPrefab, PlayerTank.position, PlayerTank.rotation) as GameObject;
                        playerOne = playerTank;
                    } else if (true && multiplayer == true && tempRoom.playerSpawn != null && maxPlayer == 1) {
                        PlayerTank = tempRoom.playerSpawn.GetComponent<Transform>();
                        maxPlayer++;
                        GameObject playerTank = Instantiate(player2Prefab, PlayerTank.position, PlayerTank.rotation) as GameObject;
                        playerTwo = playerTank;
                    }
                    else if (tempRoom.playerSpawn == null)
                    {

                    }
                    else
                    {
                        tempRoom.playerSpawn.SetActive(false);
                    }
                }


                //Time to open and close our doors in our grid on the edges and inside.
                if (i == 0)//If our room is against the south side
                {
                    tempRoom.doorNorth.SetActive(false);//turn off our north doors.
                }
                else if (i == rows - 1)//if our room is on the north side
                {
                    tempRoom.doorSouth.SetActive(false);//turn off our south doors.
                }
                else//if our room is in the middle
                {
                    tempRoom.doorNorth.SetActive(false);//turn off north doors
                    tempRoom.doorSouth.SetActive(false);//turn off south doors
                }
                if (j == 0)//if room is on the west side
                {
                    tempRoom.doorEast.SetActive(false);//turn off our east doors
                }
                else if (j == cols - 1)//if our room is on the east side
                {
                    tempRoom.doorWest.SetActive(false);//turn off our west doors
                }
                else//if our rooms are in the middle
                {
                    tempRoom.doorEast.SetActive(false);//turn off the east doors
                    tempRoom.doorWest.SetActive(false);//turn off the west doors
                }

                grid[i, j] = tempRoom;//save it to our grid array
            }
        }
    }
    //selects a random room from our list of rooms.
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];//Randomly selects from the list of Developer selected rooms.
    }
   

    //functions to swithc between "in game scenes"
    public void gameStart()
    {
        TwoPlayer();
        typeOfMap();
        GenerateGrid();//Calls our Map Generating Function.
        AudioSource.PlayClipAtPoint(clicking, MenuCamera.GetComponent<Transform>().position, fxSound.value);
        playerOneGui.SetActive(true);
        if(multiplayer == true)
        {
            playerTwoGui.SetActive(true);
        }
        MenuCamera.SetActive(false);
        startScreen.SetActive(false);
        game.SetActive(true);
        pauseMenu.SetActive(false);
        AudioSource.PlayClipAtPoint(gameMusic, MenuCamera.GetComponent<Transform>().position, musicSound.value);
    }
    public void gameSettings()
    {
        AudioSource.PlayClipAtPoint(clicking, MenuCamera.GetComponent<Transform>().position, fxSound.value);
        startScreen.SetActive(false);
        options.SetActive(true);
        AudioSource.PlayClipAtPoint(menuMusic, MenuCamera.GetComponent<Transform>().position, musicSound.value);
    }
    public void settingToMain()
    {
        AudioSource.PlayClipAtPoint(clicking, MenuCamera.GetComponent<Transform>().position, fxSound.value);
        startScreen.SetActive(true);
        options.SetActive(false);
        AudioSource.PlayClipAtPoint(menuMusic, MenuCamera.GetComponent<Transform>().position, musicSound.value);
    }
    public void pause()
    {
        AudioSource.PlayClipAtPoint(clicking, MenuCamera.GetComponent<Transform>().position, fxSound.value);
        pauseMenu.SetActive(true);
        game.SetActive(false);
        AudioSource.PlayClipAtPoint(menuMusic, MenuCamera.GetComponent<Transform>().position, musicSound.value);
    }
    public void quit()
    {
        AudioSource.PlayClipAtPoint(clicking, MenuCamera.GetComponent<Transform>().position, fxSound.value);
        Application.Quit();
    }
    public void typeOfMap()
    {
        int x = mapMode.value;
        Debug.Log(x);
        if (x == 0)
        {
            mapGen = mapGeneration.Random;
        } else if (x == 1)
        {
            mapGen = mapGeneration.TimeOfDay;
        } else
        {
            Debug.Log("Broken");
        }
    }
    public void restart()
    {
        
        AudioSource.PlayClipAtPoint(clicking, MenuCamera.GetComponent<Transform>().position, fxSound.value);
        playerOneGui.SetActive(false);
        playerTwoGui.SetActive(false);
        MenuCamera.SetActive(true);
        startScreen.SetActive(true);
        pauseMenu.SetActive(false);
        gameover.SetActive(false);
        AudioSource.PlayClipAtPoint(menuMusic, MenuCamera.GetComponent<Transform>().position, musicSound.value);
        playerOneLivesPlaceholder = playerOneLives;
        playerOneScorePlaceholder = playerOneScore;
        playerTwoLivesPlaceholder = playerTwoLives;
        playerTwoScorePlaceholder = playerTwoScore;
        maxPlayer = 0;
    }
    public void TwoPlayer()
    {
        int x = multiplayerInt.value;
        Debug.Log(x);
        if (x == 0){
            multiplayer = false;
            playerTwoLives = 0;
            
        } else if (x == 1)
        {
            multiplayer = true;
        }
    }
    public void Gameover()
    {
        gameover.SetActive(true);
        game.SetActive(false);
    }

    //cheat sheat for taking a life.
    public void TakeALife(int x)
    {
        if (x == 0)
        {
            playerOneLives--;
            playerOneLivesText.text = "Lives remaining: " + playerOneLives;
        } else if (x == 1)
        {
            playerTwoLives--;
            playerTwoLivesText.text = "Lives remaining: " + playerTwoLives;
        } else
        {

        }
    }
}

