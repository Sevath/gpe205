﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour {
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    
	}
    public void OnCollisionEnter(Collision collision)//On collision of the bullet
    {
        if (collision.gameObject.tag == "EnemyTank")//with enemy tank
        {
            Destroy(collision.gameObject);//Destroy tank
            Destroy(this.gameObject);//Destroy bullet
        }
        if (collision.gameObject.tag == "Wall")//with wall
        {
            Destroy(this.gameObject);//destroy bullet.
        }
    }
}
