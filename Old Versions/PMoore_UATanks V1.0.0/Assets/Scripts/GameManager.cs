﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public int player1Lives;//sets player 1 lives
    public int player2Lives;//sets player 2 lives
    public int player1Health;//sets player 1 health
    public int player2Health;//sets player 2 health
    public GameObject bullet;//Allows the bullet object to be set
    public float reloadDelay;//holds the timer to reload
    private float timeHolder;//holds time last bullet was fired. 
    public float bulletSpeed;//holds bullet speed
    public Transform tf;//allows transform to be publicly set for location of bullet spawn
                        // Use this for initialization
    void Awake () {
		if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this.gameObject);
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Holder()//Created a place for the GameOver function.
    {
        Debug.Log("Can not shoot");
    }
    public void WhoShot(string player)
    {
        if (Time.time >= reloadDelay + timeHolder)
        {//if user presses space and the cannon is ready
            GameObject newBullet = Instantiate(bullet, tf.GetComponent<Transform>().position, tf.GetComponent<Transform>().rotation) as GameObject;//summons the bullet
            timeHolder = Time.time;//updates last bullet fire time
            newBullet.GetComponent<Rigidbody>().velocity = bulletSpeed * tf.forward;//fires the bullet at designated designer speed
            Debug.Log(player);//Logs which player shoots
            Destroy(newBullet, 5);//destroys bullet after 5 seconds

        }
        else if (Time.time < reloadDelay + timeHolder)//if user presses space and the cannon is not ready
        {
           Holder();//Debugs when you can not shoot.
        }
    }
}
