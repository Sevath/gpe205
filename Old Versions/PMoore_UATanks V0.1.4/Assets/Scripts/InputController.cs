﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    //Put variables here
    public enum InputScheme { WASD, arrowKeys };//Allows for input to be WASD or Arrow Keys
    public InputScheme  input= InputScheme.WASD;//Defaults the movement to WASD
    public TankData data;//Grabs the TankData Script
    public TankMotor motor;//Grabs the TankMotor Script
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		switch (input)//If the input is....
        {
            case InputScheme.arrowKeys://if Input is the arrow keys
                   if (Input.GetKey(KeyCode.UpArrow))//if the user presses the Up Arrow
                {
                   motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.DownArrow))//if the user presses the down arrow
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.RightArrow))//if the user presses the right arrow 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.LeftArrow))//if the user presses the left arrow
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                break;
        }
	}
}
