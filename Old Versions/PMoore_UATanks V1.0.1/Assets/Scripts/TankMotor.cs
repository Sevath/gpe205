﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {
    //put variables here
    private CharacterController characterController;//Creates a variable for the CharacterController
    public Transform tf;//creates a vriable for the Transform
	// Use this for initialization
	void Start () {
        characterController = gameObject.GetComponent<CharacterController>();//Grabs the CharacterController.
        tf = gameObject.GetComponent<Transform>();//Grabs the Transform Component
	}
	
	// Update is called once per frame
	void Update () {

	}
    //Self created functions below.
    public void Move(float speed)//Creates a function for moving forward
    {
        Vector3 speedVector = tf.forward;//Creates a speed vector, and places the facing for forward of the object.
        characterController.SimpleMove (speedVector *= speed);//applys the speed value, and concerts it to meters per second.
    }
    public void Rotate(float speed)//Create a function for roation
    {
        Vector3 rotateVector = Vector3.up;//sets the roation by one degree per frame.
        rotateVector *= speed * Time.deltaTime;//Adjusts rotation based on speed changes rotation to speed instead of frames.
        tf.Rotate(rotateVector, Space.Self);//rotates the tank by the set value.
    }
}
