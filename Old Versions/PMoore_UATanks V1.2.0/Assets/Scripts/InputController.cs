﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    //Put variables here
    public enum InputScheme { WASD, arrowKeys };//Allows for input to be WASD or Arrow Keys
    public InputScheme  input= InputScheme.WASD;//Defaults the movement to WASD
    public TankData data;//Grabs the TankData Script
    public TankMotor motor;//Grabs the TankMotor Script
    public string player;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		switch (input)//If the input is....
        {
            case InputScheme.arrowKeys://if Input is the arrow keys
                player = "ArrowKey Player";
                if (Input.GetKey(KeyCode.UpArrow))//if the user presses the Up Arrow
                {
                   motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.DownArrow))//if the user presses the down arrow
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.RightArrow))//if the user presses the right arrow 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.LeftArrow))//if the user presses the left arrow
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                if (Input.GetKeyDown(KeyCode.RightControl))//if user presses right cntrl
                {
                    GameManager.instance.WhoShot(player);//set the player as one.
                }
                break;
            case InputScheme.WASD:
                player = "WASD Player";
                if (Input.GetKey(KeyCode.W))//if the user presses W
                {
                    motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.S))//if the user presses S
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.D))//if the user presses D 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.A))//if the user presses A
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                if (Input.GetKeyDown(KeyCode.Space))//if user presses space
                {
                    GameManager.instance.WhoShot(player);//set the player number for two.
                }
                break;
        }
	}
}
