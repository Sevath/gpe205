﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : MonoBehaviour {
    public enum LoopType { Stop, Loop, PingPong };
    public LoopType loopType;
    private bool isPatrolForward = true;
    public Transform[] waypoints;//allows addition of waypoints for tank patrol
    public TankMotor motor;//allows TankMotor script to be accessed
    public TankData data;//allows TankData script to be accessed
    private int currentWaypoint = 0;//sets first waypoint as 0
    public float closeEnough = 1.0f;//allows set close enough before next waypoints
    private Transform tf;
    // Use this for initialization
    private void Awake()
    {
        tf = gameObject.GetComponent<Transform>();//grabs the transform of our game object.
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.rotateSpeed))//rotate towards our waypoint
        {
            //do nothing
        } else
        {
            motor.Move(data.moveSpeed);//move to the waypoint
        }
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))//if we are close enough the the current waypoint
        {
            if (loopType == LoopType.Stop) {
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
            }else if(loopType == LoopType.Loop)
            {
                currentWaypoint++;
                if (currentWaypoint == waypoints.Length)//if our waypoint hits above max
                {
                    currentWaypoint = 0;//reset our movement.
                }
            }else if (loopType == LoopType.PingPong)
            {
                if (isPatrolForward)
                {
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        isPatrolForward = false;
                        currentWaypoint--;
                    }
                }
                else
                {
                    if (currentWaypoint > 0)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        isPatrolForward = true;
                        currentWaypoint++;
                    }
                }
            }
        }
    }
}
