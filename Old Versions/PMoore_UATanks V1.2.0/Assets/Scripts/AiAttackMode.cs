﻿//USE THIS SCRIPT FOR TESTING REASONS!!!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiAttackMode : MonoBehaviour {
    public enum AttackMode { Chase, Flee, none };//Developer can choose attack mode
    public AttackMode attackMode;//sets the attack mode as attack mode
    public Transform target;//allows us to manually set the target
    private Transform tf;//holds current GameObject Transform Component
    private TankData data;//Holds the TankData script
    private TankMotor motor;//Holds the Tank Motor script
    public float fleeDistance = 1.0f;//Publicly sets the fleeDistance we will run
    private int avoidanceStage = 0;//allows programmer to set the avoidance stage we will be in
    public float avoidanceTime = 2.0f;//publicly allows how far in the future we will look into
    private float exitTime;

    // Use this for initialization
    void Start () {
        data = GetComponent<TankData>();//grabs the TankData script
        motor = GetComponent<TankMotor>();//grabs the TankMotor script
        tf = GetComponent<Transform>();//Grabs the Transform component
	}
	
	// Update is called once per frame
	void Update () {
        if (attackMode == AttackMode.none)//If attack mode is none
        {
            //Do nothing
        }
		else if (attackMode == AttackMode.Chase)//If we are going to chase
        {
            if (avoidanceStage != 0)//and our avoidance stage is not 0
            {
                DoAvoidance();//avoid the obsticle
            }
            else//otherwise
            {
                DoChase();//chase the target
            }
        } else if (attackMode == AttackMode.Flee){//if tge attack mode is set to flee.
            Vector3 vectorToTarget = target.position - tf.position;//how far away are we from the target?
            Vector3 vectorAwayFromTarget = -1 * vectorToTarget;//move away from the target.
            vectorAwayFromTarget.Normalize();
            vectorAwayFromTarget *= fleeDistance;
            Vector3 fleePosition = vectorAwayFromTarget + tf.position;
            motor.RotateTowards(fleePosition, data.rotateSpeed);//rotate away from the target
            motor.Move(data.moveSpeed);//move forward
        }
	}
    void DoChase()//When we need to chase
    {
        motor.RotateTowards(target.position, data.rotateSpeed);//rotate to the target
        if (CanMove(data.moveSpeed))//if nothing is infront of us
        {
            motor.Move(data.moveSpeed);//move forward
        }
        else//otherwise
        {
            avoidanceStage = 1;//Change the avoidance stage to 1
        }
    }

    void DoAvoidance()//when we need to avoid an obsticle
    {
        if (avoidanceStage == 1)
        {
            motor.Rotate(-1 * data.rotateSpeed);
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;
                exitTime = avoidanceTime;
            }
        }
        else if (avoidanceStage == 2)
        {
            if (CanMove(data.moveSpeed))
            {
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            } else
            {
                avoidanceStage = 1;
            }
        }
    }
    bool CanMove(float speed)
    {
        RaycastHit hit;
        if (Physics.Raycast (tf.position, tf.forward, out hit, speed)) {
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }
        return true;
    }
}
