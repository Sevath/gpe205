﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {
    //put variables here
    private CharacterController characterController;
    public Transform tf;
	// Use this for initialization
	void Start () {
        characterController = gameObject.GetComponent<CharacterController>();
        tf = gameObject.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {

	}
    public void Move(float speed)//Creates a function for moving forward
    {
        Vector3 speedVector = tf.forward;
        characterController.SimpleMove(speedVector *= speed);//applys the speed value, and voncerts it to meters per second.
    }
    public void Rotate(float speed)//Create a function for roation
    {
        Vector3 rotateVector = Vector3.up;//sets the roation by one degree per frame.
        rotateVector *= speed * Time.deltaTime;//Adjusts rotation based on speed changes rotation to speed instead of frames.
        tf.Rotate(rotateVector, Space.Self);//rotates the tank by the set value.
    }
}
