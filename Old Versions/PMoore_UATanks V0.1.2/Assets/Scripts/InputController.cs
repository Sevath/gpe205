﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    public enum InputScheme { WASD, arrowKeys };
    public InputScheme  input= InputScheme.WASD;
    public TankData data;
    public TankMotor motor;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		switch (input)
        {
            case InputScheme.arrowKeys:
                   if (Input.GetKey(KeyCode.UpArrow))
                {
                   motor.Move(3);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    motor.Move(-3);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    motor.Rotate(data.rotateSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    motor.Rotate(-data.rotateSpeed);
                }
                break;
        }
	}
}
