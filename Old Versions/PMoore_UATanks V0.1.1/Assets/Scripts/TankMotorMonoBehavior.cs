﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotorMonoBehavior : MonoBehaviour {
    //put variables here
    private CharacterController characterController; //This variable holds the character controller component.
	// Use this for initialization
	void Start () {
        characterController = gameObject.GetComponent<CharacterController>();//Store the characterController in our variable
	}
	
	// Update is called once per frame
	void Update () {

	}
    public void Move(float speed)//Creates a function for moving forward
    {
        Vector3 speedVector = transform.forward;//Create a vector3 fir soeedVector, with a direction  of the front of the tank.
        characterController.SimpleMove(speedVector *= speed);//applys the speed value, and voncerts it to meters per second.
    }
    public void Rotate(float speed)//Create a function for roation
    {
        Vector3 rotateVector = Vector3.up;//sets the roation by one degree per frame.
        rotateVector *= speed;//Adjusts rotation based on speed
        rotateVector *= Time.deltaTime;//changes rotation to speed instead of frames.
        transform.Rotate(rotateVector, Space.Self);//rotates the tank by the set value.
    }
}
