﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public int player1Lives;//sets player 1 lives
    public int player2Lives;//sets player 2 lives
    public int player1Health;//sets player 1 health
    public int player2Health;//sets player 2 health
	// Use this for initialization
	void Awake () {
		if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this.gameObject);
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void GameOver()//Created a place for the GameOver function.
    {
        Debug.Log("Testing");
    }
}
