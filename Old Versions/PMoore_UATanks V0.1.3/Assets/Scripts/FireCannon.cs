﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCannon : MonoBehaviour {
    public GameObject bullet;//Allows the bullet object to be set
    public float reloadDelay;//holds the timer to reload
    private float timeHolder;//holds time last bullet was fired. 
	// Use this for initialization
	void Start () {
        timeHolder = Time.time;//grabs start of the game.
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)&&Time.time>=reloadDelay + timeHolder) {//if user presses space and the cannon is ready
            GameObject newBullet = Instantiate(bullet, GetComponent<Transform>().position, GetComponent<Transform>().rotation) as GameObject;//summons the bullet
            timeHolder = Time.time;//updates last bullet fire time
            Destroy(newBullet, 5);//destroys bullet after 5 seconds
        } else if (Input.GetKeyDown(KeyCode.Space) && Time.time < reloadDelay + timeHolder)//if user presses space and the cannon is not ready
        {
            GameManager.instance.GameOver();//Testing grabbing methods from GameManager.
        }
	}
}
