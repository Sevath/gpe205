﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {
    public Powerup powerup;
    public AudioClip feedback;
    private Transform tf;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter(Collider other)
    {
        PowerupController Powcon = other.GetComponent<PowerupController>();
            if (Powcon != null)
        {
            Powcon.Add(powerup);
            if (feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, tf.position, 1.0f);
            }
            Destroy(gameObject);
        }
    }
}
