﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour {
    public List<Powerup> powerups;//Creates our power up list variable
    private TankData tankData;//grabs the TankData script
	// Use this for initialization
	void Start () {
        powerups = new List<Powerup>();//starts our list for the powerups
        tankData = GetComponent<TankData>();//Grabs our TankData script
	}
	
	// Update is called once per frame
	void Update () {
        List<Powerup> expiredPowerups = new List<Powerup>();
        foreach (Powerup power in powerups)
        {
            power.duration -= Time.deltaTime;
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }
        foreach (Powerup power in expiredPowerups)
        {
            power.OnDeactivate(tankData);
            powerups.Remove(power);
        }
	}
    public void Add (Powerup powerup)
    {
        powerup.OnActivate(tankData);
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }
}
