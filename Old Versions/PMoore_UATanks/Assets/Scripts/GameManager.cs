﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public int playerKeyArrowScore;
    public int playerWASDScore;
    public GameObject bullet;//Allows the bullet object to be set
    public float timeHolder;//holds time last bullet was fired. 
    public float bulletSpeed;//holds bullet speed
    public float bulletDecay;//Public variable for when the bullet will despawn.
    public Transform tf;//allows transform to be publicly set for location of bullet spawn
    public FSM Ai;//Allows us to set the FSM for the ai
    public TankData data;//grabs our TankData component.
    //room prefabs
    public int rows;
    public int cols;
    public float roomWidth = 50.0f;
    public float roomHeight = 50.0f;
    public GameObject[] gridPrefabs;//Allows us to set which tiles we will use in our map
    private Room[,] grid;//saves a list of our current map tiles we created later on
    public Transform PlayerTank;//
    public GameObject EnemyTank;
    public Transform[] enemySpawn;
    public enum mapGeneration { Random, Preset, TimeOfDay }; //allows developers to set which kind of map they wish to spawn
    public mapGeneration mapGen = mapGeneration.Preset;//auto sets it for developer specific
    public int mapSeed;//gives a specific map seed to be allowed

    //helps with spawning in enemys and ourselves randomly.
    public int MaxEnemy;
    private int CurEnemy;
    private bool maxPlayer = false;

    // Use this for initialization
    void Awake () {
		if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this.gameObject);
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
	}
    //When the game starts
    void Start()
    {
        GenerateGrid();
    }

    // Update is called once per frame
    void Update () {
	}
    public void WhoShot(string player)
    {
        if (Time.time >= data.fireRate + timeHolder)
        {//if the cannon is ready
            GameObject newBullet = Instantiate(bullet, tf.GetComponent<Transform>().position, tf.GetComponent<Transform>().rotation) as GameObject;//summons the bullet
            timeHolder = Time.time;//updates last bullet fire time
            newBullet.GetComponent<Rigidbody>().velocity = bulletSpeed * tf.forward;//fires the bullet at designated designer speed
            Debug.Log(player);//Logs which player shoots
            Destroy(newBullet, bulletDecay);//destroys bullet after 5 seconds

        }
        else if (Time.time < data.fireRate + timeHolder)//if user presses space and the cannon is not ready
        {
           //Do nothing.
        }
    }
    public void Score(string player)
    {
        if (player == "ArrowKey Player")
        {
            playerKeyArrowScore++;
            Debug.Log(playerKeyArrowScore + " testing one");
        }
        else if(player == "WASD Player")
        {
            playerWASDScore++;
            Debug.Log(playerWASDScore + " Testing two");
        }
    }
    //Create a random seed using Time!
    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;//adds all the time used for today to create a random map.
    }
    //generates our map.
    public void GenerateGrid()
    {
        switch (mapGen)//This allows us to decide how random our map will truly be.
        {
            case mapGeneration.Preset://if developer set it as preset
                UnityEngine.Random.InitState(mapSeed);//use preset seed determained
                break;
            case mapGeneration.Random://if set as random
                UnityEngine.Random.InitState(DateToInt(DateTime.Now));//use all of the function DatetoInt to create random seed to spawn.
                break;
            case mapGeneration.TimeOfDay://if time of day.
                mapSeed = DateToInt(DateTime.Now.Date);//we will set our current seed as today using the function DateTime for now and todays date.
                UnityEngine.Random.InitState(mapSeed);//spawns the map using our new seed.
                break;
        }
        //Clear our grid
        grid = new Room[cols, rows];
        for (int i = 0; i < rows; i++)//for each grid row
        {
            for (int j = 0; j < cols; j++)//and for each grip column
            {
                //figure out which section in our grip
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);
                //creates a random room from our prefabs at that location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;
                //sets it as a parent
                tempRoomObj.transform.parent = this.transform;
                //names our new grip location.
                tempRoomObj.name = "Room_" + j + "," + i;
                //grab our room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //this will set a random Bool for our enemy spawns.
                    bool Boolean = (UnityEngine.Random.value > 0.5f);
                //if true and we hav eyet hit the max enemy limit
                if (Boolean == true && CurEnemy < MaxEnemy)
                {
                    //spawn in a new enemy
                    tempRoom.EnemySpawn.SetActive(true);
                    CurEnemy++;
                }
                //otherwise do not spawn our enemy.
                else
                {
                    tempRoom.EnemySpawn.SetActive(false);
                }

                //repeat but with our player selection.
                bool BooleanPlayer = (UnityEngine.Random.value > 0.5f);
                if (BooleanPlayer == true && maxPlayer == false && tempRoom.playerSpawn != null)
                {
                    maxPlayer = true;
                    tempRoom.playerSpawn.SetActive(true);
                    PlayerTank = tempRoom.playerSpawn.GetComponent<Transform>();
                } else if (tempRoom.playerSpawn == null)
                {

                }else 
                {
                    tempRoom.playerSpawn.SetActive(false);
                }
                //Time to open and close our doors in our grid on the edges and inside.
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                } else if (i == rows - 1)
                {
                    tempRoom.doorSouth.SetActive(false);
                } else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                } else if (j == cols - 1)
                {
                    tempRoom.doorWest.SetActive(false);
                } else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
                //save it to our grid array
                grid[i, j] = tempRoom;
            }
        }
    }
    //selects a random room from our list of rooms.
    public GameObject RandomRoomPrefab()//names our function
    {
        //Randomly selects from the list of Developer selected rooms.
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }
}
