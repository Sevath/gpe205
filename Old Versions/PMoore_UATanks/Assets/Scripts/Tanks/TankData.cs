﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {
    //variables here
    public float moveSpeed;//Controls how fast the tank will move forward/ backwards
    public float rotateSpeed;//Controls how fast the tank will rotate.
    public bool shield = false;//shield variable set false
    public float fireRate;//how fast we can shoot.
    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
