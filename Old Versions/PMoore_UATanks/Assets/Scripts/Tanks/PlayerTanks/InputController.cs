﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    //Put variables here
    public float timeHolder;//holds time last bullet was fired. 
    public float bulletSpeed;//holds bullet speed
    public float bulletDecay;//Public variable for when the bullet will despawn.
    public Transform tf;//allows transform to be publicly set for location of bullet spawn
    public GameObject bullet;//Allows the bullet object to be set
    public enum InputScheme { WASD, arrowKeys };//Allows for input to be WASD or Arrow Keys
    public InputScheme  input= InputScheme.WASD;//Defaults the movement to WASD
    public TankData data;//Grabs the TankData Script
    public TankMotor motor;//Grabs the TankMotor Script
    public string player;
    public float volume;
    public GameManager gm;
    // Use this for initialization
    void Start () {
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
	}
	
	// Update is called once per frame
	void Update () {
		switch (input)//If the input is....
        {
            case InputScheme.arrowKeys://if Input is the arrow keys
                player = "ArrowKey Player";
                if (Input.GetKey(KeyCode.UpArrow))//if the user presses the Up Arrow
                {
                   motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.DownArrow))//if the user presses the down arrow
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.RightArrow))//if the user presses the right arrow 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.LeftArrow))//if the user presses the left arrow
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                if (Input.GetKeyDown(KeyCode.RightControl))//if user presses right cntrl
                {
                    if (Time.time >= data.fireRate + timeHolder)
                    {//if the cannon is ready
                        GameObject newBullet = Instantiate(bullet, tf.GetComponent<Transform>().position, tf.GetComponent<Transform>().rotation) as GameObject;//summons the bullet
                        timeHolder = Time.time;//updates last bullet fire time
                        newBullet.GetComponent<Rigidbody>().velocity = bulletSpeed * tf.forward;//fires the bullet at designated designer speed
                        Debug.Log(player);//Logs which player shoots
                        Destroy(newBullet, bulletDecay);//destroys bullet after 5 seconds

                    }
                    else if (Time.time < data.fireRate + timeHolder)//if user presses space and the cannon is not ready
                    {
                        //Do nothing.
                    }
                }
                break;
            case InputScheme.WASD:
                player = "WASD Player";
                if (Input.GetKey(KeyCode.W))//if the user presses W
                {
                    motor.Move(data.moveSpeed);//Move forward
                }
                if (Input.GetKey(KeyCode.S))//if the user presses S
                {
                    motor.Move(-data.moveSpeed);//move backwards
                }
                if (Input.GetKey(KeyCode.D))//if the user presses D 
                {
                    motor.Rotate(data.rotateSpeed);//move right
                }
                if (Input.GetKey(KeyCode.A))//if the user presses A
                {
                    motor.Rotate(-data.rotateSpeed);//move left.
                }
                if (Input.GetKeyDown(KeyCode.Space))//if user presses space
                {
                    if (Time.time >= data.fireRate + timeHolder)
                    {//if the cannon is ready
                        GameObject newBullet = Instantiate(bullet, tf.GetComponent<Transform>().position, tf.GetComponent<Transform>().rotation) as GameObject;//summons the bullet
                        timeHolder = Time.time;//updates last bullet fire time
                        newBullet.GetComponent<Rigidbody>().velocity = bulletSpeed * tf.forward;//fires the bullet at designated designer speed
                        Debug.Log(player);//Logs which player shoots
                        Destroy(newBullet, bulletDecay);//destroys bullet after 5 seconds

                    }
                    else if (Time.time < data.fireRate + timeHolder)//if user presses space and the cannon is not ready
                    {
                        //Do nothing.
                    }
                }
                break;
        }
	}
}
