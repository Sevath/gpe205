﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour {
    // Use this for initialization
    public string player;
    void Start()
    {
        if ("WASD Player" == GameObject.FindWithTag("Player").GetComponent<InputController>().player)
        {
            player = "WASD Player";
        }
        else if (GameObject.FindWithTag("Player").GetComponent<InputController>().player == "ArrowKey Player")
            player = "ArrowKey Player";
    }
	
	// Update is called once per frame
	void Update () {
    
	}
    public void OnCollisionEnter(Collision collision)//On collision of the bullet
    {
        if (collision.gameObject.tag == "EnemyTank")//with enemy tank
        {
            Destroy(this.gameObject);//Destroy bullet
            Destroy(collision.gameObject);//Destroy tank
            GameManager.instance.Score(player);
        }
        if (collision.gameObject.tag == "Wall")//with wall
        {
            Destroy(this.gameObject);//destroy bullet.
        }
    }
}
